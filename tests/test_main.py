import itertools
import pytest
from urllib.parse import urlparse


def disable_event_save(mocker):
    mocker.patch('app.main.Event._save_enabled',
                 new_callable=mocker.PropertyMock, return_value=False)


@pytest.mark.parametrize('camera_id, image_name',
                         itertools.product(['1', '2'], ['person', 'car']))
@pytest.mark.integration
def test_trigger(camera_id, image_name, client, httpserver, test_image, config, mocker):
    disable_event_save(mocker)
    cameraname = config.camera[camera_id]['name']
    x_trigger = urlparse(config.camera[camera_id]['trigger_url']).path

    httpserver.expect_request('/webapi/entry.cgi', method='GET').\
        respond_with_data(test_image(image_name), content_type='image/jpeg')
    httpserver.expect_request(x_trigger, method='GET').respond_with_data('OK')

    response = client.get(f'/{camera_id}')

    assert response.status_code == 200
    assert f'Camera {camera_id} ({cameraname}): recording; {image_name} found' in response.text
    assert f'Capture saved to ' in response.text
    httpserver.check_assertions()


@pytest.mark.parametrize('image_name', ['person', 'car'])
@pytest.mark.integration
def test_ignore(image_name, client, httpserver, test_image, config, mocker):
    disable_event_save(mocker)
    camera_id = '3'
    cameraname = config.camera[camera_id]['name']
    x_response = f'Camera {camera_id} ({cameraname}): ignoring movement; area is ignored'

    httpserver.expect_request('/webapi/entry.cgi', method='GET').\
        respond_with_data(test_image(image_name), content_type='image/jpeg')

    response = client.get(f'/{camera_id}')

    assert response.status_code == 200
    assert x_response in response.text
    httpserver.check_assertions()


@pytest.mark.integration
def test_empty(client, httpserver, test_image, config, mocker):
    disable_event_save(mocker)
    camera_id = '1'
    cameraname = config.camera[camera_id]['name']
    x_response = f'Camera {camera_id} ({cameraname}): ignoring movement; nothing detected'

    httpserver.expect_request('/webapi/entry.cgi', method='GET').\
        respond_with_data(test_image('blank'), content_type='image/jpeg')

    response = client.get(f'/{camera_id}')

    assert response.status_code == 200
    assert x_response in response.text
    httpserver.check_assertions()


@pytest.mark.integration
def test_stationary(client, httpserver, test_image, config, mocker):
    mocker.patch('app.main.Event._processing_suspended',
                 new_callable=mocker.PropertyMock, return_value=False)

    camera_id = '1'
    cameraname = config.camera[camera_id]['name']
    x_response = f'Camera {camera_id} ({cameraname}): ignoring movement; person is stationary'

    httpserver.expect_request('/webapi/entry.cgi', method='GET').\
        respond_with_data(test_image('person'), content_type='image/jpeg')
    client.get(f'/{camera_id}')  # save as previous event
    response = client.get(f'/{camera_id}')

    assert response.status_code == 200
    assert x_response in response.text
