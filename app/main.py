from fastapi import FastAPI

import requests
import logging
import os

from .camera import Camera
from .config import Config
from .event import Event, EventError
from .synology import SynologySession

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%dT%H:%M:%S%z')
LOG = logging.getLogger('app')  # need the root logger for the whole app, not just 'app.main'
LOG.setLevel(logging.getLevelName(os.environ.get("LOG_LEVEL", "INFO").upper()))
LOG.info(f'App Started logger: {LOG.name}')

APP = FastAPI()
CONFIG = Config()

LOG.info(f"Synology login to {CONFIG.settings['sss_url']}")
SYNOLOGY_SESSION = SynologySession(
    CONFIG.settings['sss_url'], CONFIG.settings["username"], CONFIG.settings["password"])


def with_log(msg, log_level=LOG.info):
    log_level(msg)
    return msg


def homebridge_trigger(camera: Camera):
    homebridge_webhook_url = CONFIG.settings["homebridge_webhook_url"]
    if homebridge_webhook_url and camera.homekit_acc_id:
        hb = requests.get(f"{homebridge_webhook_url}/?accessoryId={camera.homekit_acc_id}&state=true")
        LOG.debug(f"Sent message to homebridge webhook: {hb.status_code}")


@APP.get("/{camera_id}")
async def read_item(camera_id):
    try:
        event = Event(
            config=CONFIG,
            synology_session=SYNOLOGY_SESSION,
            camera_id=camera_id,
        )
    except EventError as e:
        return with_log(str(e), log_level=LOG.error)

    extra_information = ''
    if event.recording:
        homebridge_trigger(event.camera)
        # TODO: log what was found in the image
        extra_information = f'. Capture saved to "{event.capture_image_path}"'

    return with_log(event.camera.msg(
        f'{event.action}; {event.action_reason} '
        f'(analysed in {event.elapsed_time:.1f}s){extra_information}'))
