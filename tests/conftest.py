import pathlib
import pytest
from fastapi.testclient import TestClient

from app.camera import Camera

IMAGES_PATH = pathlib.Path('tests/fixtures/images')


@pytest.fixture(scope='session')
def camera():
    return Camera('1', 'Camera 1', 'http://localhost', [])


@pytest.fixture(scope='session')
def config():
    from app.config import Config
    return Config()


@pytest.fixture
def test_image():
    def fixture(name):
        return open(IMAGES_PATH.joinpath(name).with_suffix('.jpg'), mode='rb').read()
    return fixture


@pytest.fixture(scope='function')
def client(httpserver):
    httpserver.expect_oneshot_request('/webapi/auth.cgi', method='GET').respond_with_data('OK')

    from app.main import APP as fast_api_app

    httpserver.check_assertions()

    return TestClient(fast_api_app)


@pytest.fixture(scope="session")
def httpserver_listen_address():
    return '0.0.0.0', 8000
