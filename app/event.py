from __future__ import annotations

import json
import logging
import pathlib
import pickle
import time
from dataclasses import dataclass, field
from typing import Optional
from urllib.parse import urljoin

import requests
from PIL import Image, ImageDraw

from .config import Config
from .camera import Camera, CameraCollection, Areas, Area
from .synology import Snapshot, SynologySession

LOG = logging.getLogger(__name__)
DATA_PATH_PREFIX = pathlib.Path('events')

ACTION_IGNORING_MOVEMENT = 'ignoring movement'

REASON_TRIGGER_INTERVAL = 'waiting for trigger_interval'
REASON_NO_MATCH = 'no match for detect_labels'
REASON_NOTHING_DETECTED = 'nothing detected'


class EventError(Exception):
    pass


def deepstack_detection(image, settings):
    timeout = settings['timeout']
    deepstack_url = urljoin(settings['deepstack_url'], '/v1/vision/detection')
    try:
        s = time.perf_counter()
        response = requests.post(f"{deepstack_url}", files={"image": image}, timeout=timeout).json()
        e = time.perf_counter()
        LOG.debug('Deepstack result: %s. Time: %ds', json.dumps(response, indent=2), (e-s))
    except (json.decoder.JSONDecodeError, requests.exceptions.ConnectionError) as e:
        LOG.error(e)
        return None

    if (predictions := response.get('predictions')) is None:
        LOG.warning('Deepstack error: %s', response)
        return []

    return predictions


def prediction_size(prediction):
    sizex = int(prediction["x_max"]) - int(prediction["x_min"])
    sizey = int(prediction["y_max"]) - int(prediction["y_min"])
    return sizex, sizey


@dataclass
class Event:
    config: Config
    synology_session: SynologySession
    camera_id: str

    settings: dict = None
    cameras: CameraCollection = None
    camera: Camera = None
    snapshot: Snapshot = None
    action: str = ACTION_IGNORING_MOVEMENT
    action_reason: str = ''
    predictions: list[dict] = field(default_factory=list)
    recording: bool = False
    start_time: float = None
    end_time: float = None
    _save_enabled: bool = True
    _stationary_objects: Areas = field(default_factory=list)

    def __post_init__(self):
        self.start_time = time.time()
        self.cameras = CameraCollection(self.config.camera)
        self.settings = self.config.settings

        try:
            self.camera = self.cameras[self.camera_id]
        except KeyError:
            raise EventError(f'Configuration for camera {self.camera_id} not found') from None

        if self._processing_suspended:
            self.end_time = time.time()
            self.action_reason = f'{REASON_TRIGGER_INTERVAL} in {self.trigger_time_remaining:.1f}s'
            return

        self._take_snapshot()
        self._find_predictions()
        self.end_time = time.time()
        self.save()

    @property
    def capture_image_path(self):
        capture_path = pathlib.Path(self.settings['capture_dir'])
        # TODO use timestamp from Synology
        time_format = '%Y-%m-%dT%H:%M:%S'
        start_time = time.strftime(time_format, time.localtime(self.start_time))
        file_path = capture_path.joinpath(
            f'{start_time}-camera-{self.camera.id}-{self.camera.name}').with_suffix('.jpg')
        file_path.parent.mkdir(parents=True, exist_ok=True)
        return file_path

    def _save_image(self):
        im = Image.open(self.snapshot.file_name)
        draw = ImageDraw.Draw(im)

        self._draw_predictions(draw)
        self._draw_areas(draw, self.camera.ignore_areas)
        self._draw_areas(draw, self._stationary_objects, 'stationary')

        im.save(self.capture_image_path, quality=100)
        im.close()

    def _draw_predictions(self, draw):
        for prediction in self.predictions:
            confidence = round(100 * prediction["confidence"])
            label = f"{prediction['label']} ({confidence}%)"
            draw.rectangle((prediction["x_min"], prediction["y_min"], prediction["x_max"],
                            prediction["y_max"]), outline=(255, 230, 66), width=2)
            draw.text((prediction["x_min"] + 10, prediction["y_min"] + 10),
                      f"{label}", fill=(255, 230, 66))

    @staticmethod
    def _draw_areas(draw, areas, comment='ignore', colour=(255, 66, 66)):
        for area in areas:
            draw.rectangle((area.x_min, area.y_min,
                            area.x_max, area.y_max), outline=colour, width=2)
            draw.text((area.x_min + 10, area.y_min + 10), comment, fill=colour)

    def _prediction_already_existed(self, prediction):
        def close_enough(p1, p2, key):
            if isinstance(p1.get(key), int) and isinstance(p2.get(key), int):
                factor = self.settings['stationary_check_factor']
                return abs((p1.get(key) - p2.get(key))) <= p1.get(key) * factor
            return p1.get(key) == p2.get(key)

        compare_keys = ['label', 'x_min', 'x_max', 'y_min', 'y_max']

        LOG.debug(self.camera.msg(f'Loading previous predictions to compare with {prediction}'))

        if (previous_event := self.load_previous()) is None:
            return False

        for previous_prediction in previous_event.predictions:
            if all(close_enough(prediction, previous_prediction, key) for key in compare_keys):
                return True
        return False

    def _should_save(self):
        labels = ', '.join([p.get('label') for p in self.predictions])
        self.action_reason = f'{REASON_NO_MATCH} ({labels})'
        for prediction in self.predictions:
            if self._found_something(prediction):
                if self._prediction_already_existed(prediction):
                    LOG.debug(self.camera.msg(f"object is stationary {prediction}"))
                    self.action_reason = f"{prediction['label']} is stationary"
                    self._stationary_objects.append(Area.from_dict(prediction))
                else:
                    LOG.debug(self.camera.msg(f"found something {prediction}"))
                    return True

        return False

    def _found_something(self, prediction):
        min_confidence = self.settings['min_confidence']
        detection_labels = self.settings['detection_labels']
        confidence = round(100 * prediction["confidence"])
        label = prediction["label"]

        def _fits_size():
            return prediction_size(prediction)[0] > self.settings['min_sizex'] and \
                   prediction_size(prediction)[1] > self.settings['min_sizey']

        LOG.debug('%s (%d%%) %dx%d fits=%s label=%s confidence=%s ignored=%s',
                  label, confidence, prediction_size(prediction)[0],
                  prediction_size(prediction)[1], _fits_size(), (label in detection_labels),
                  (confidence > min_confidence), self.camera.is_ignored(prediction))

        if self.camera.is_ignored(prediction):
            self.action_reason = 'area is ignored'
            return False

        found = (label in detection_labels
                 and _fits_size()
                 and confidence > min_confidence)
        if found:
            self.action_reason = f'{label} found ({confidence}% confidence)'
            LOG.debug(self.camera.msg(self.action_reason))

        return found

    def _find_predictions(self):
        if predictions := deepstack_detection(self.snapshot.image_data, self.settings):
            self.predictions = predictions
            if self._should_save():
                self.recording = True
                self.action = 'recording'
                requests.get(self.camera.trigger_url)
                self._save_image()
        else:
            self.action_reason = REASON_NOTHING_DETECTED

    def _take_snapshot(self):
        LOG.debug(self.camera.msg('requesting snapshot'))
        self.snapshot = self.synology_session.snapshot(self.camera.id)
        if self.snapshot is None:
            raise EventError(self.camera.msg('failed to get snapshot'))

    @property
    def elapsed_time(self):
        return self.end_time - self.start_time

    @property
    def _time_since_last_event(self):
        if timestamp := self._last_trigger:  # assignment because last_trigger reads from disk
            return self.start_time - timestamp
        return self.settings['trigger_interval'] + 1

    @property
    def _processing_suspended(self):
        LOG.debug(self.camera.msg(
            f"time since last event is {self._time_since_last_event:.1f}s "
            f"threshold is {self.settings['trigger_interval']:.1f}s"))
        return self._time_since_last_event <= self.settings['trigger_interval']

    @property
    def data_path(self) -> pathlib.Path:
        return pathlib.Path(self.settings['work_dir']).joinpath(DATA_PATH_PREFIX)

    @property
    def _last_event_path(self) -> pathlib.Path:
        dst_path = self.data_path.joinpath(self.camera.id)
        try:
            dst_path.parent.mkdir(parents=True, exist_ok=True)
        except OSError as e:
            LOG.error('Disabling features that rely on previous event. %s', e)
            self._save_enabled = False

        return dst_path

    def save(self):
        if self._save_enabled:
            with self._last_event_path.open('wb') as file_handle:
                LOG.debug('Saving event to %s: %s', self._last_event_path, str(self))
                pickle.dump(self, file_handle)

    def load_previous(self) -> Optional[Event]:
        if self._save_enabled:
            if self._last_event_path.exists():
                LOG.debug('Loading event from %s', self._last_event_path)
                with self._last_event_path.open('rb') as f:
                    try:
                        return pickle.load(f)
                    except (IOError, OSError, EOFError, pickle.PickleError,
                            pickle.UnpicklingError) as e:
                        LOG.error(e)
        return None

    @property
    def _last_trigger(self):
        if (last_event := self.load_previous()) is None:
            return None

        return last_event.start_time

    @property
    def trigger_time_remaining(self):
        return self.settings['trigger_interval'] - self._time_since_last_event
