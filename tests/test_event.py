from app.event import Event
from app import event as app_event


def test_action_reason_1(mocker, camera):
    mocker.patch('app.event.CameraCollection', return_value=[camera])
    mocker.patch(
        'app.event.Event.trigger_time_remaining', new_callable=mocker.PropertyMock, return_value=0)
    mocker.patch('app.event.Event._processing_suspended',
                 new_callable=mocker.PropertyMock, return_value=True)

    event = Event(mocker.Mock(name='config'), None, 0)

    assert event.action == app_event.ACTION_IGNORING_MOVEMENT
    assert event.action_reason == app_event.REASON_TRIGGER_INTERVAL + f' in 0.0s'


def test_action_reason_2(mocker, camera):
    mocker.patch('app.event.CameraCollection', return_value=[camera])
    mocker.patch('app.event.deepstack_detection', return_value=[])
    mocker.patch('app.event.Event._take_snapshot')
    mocker.patch('app.event.Event.save')
    mocker.patch('app.event.Event.snapshot', new_callable=mocker.PropertyMock)
    mocker.patch('app.event.Event._processing_suspended',
                 new_callable=mocker.PropertyMock, return_value=False)

    event = Event(mocker.Mock(name='config'), None, 0)

    assert event.action == app_event.ACTION_IGNORING_MOVEMENT
    assert event.action_reason == app_event.REASON_NOTHING_DETECTED
