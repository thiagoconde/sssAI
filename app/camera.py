import logging
from dataclasses import dataclass
from typing import Dict, List

LOG = logging.getLogger(__name__)  # app.synology


@dataclass
class Area:
    x_min: int
    x_max: int
    y_min: int
    y_max: int

    def __post_init__(self):
        if self.x_min > self.x_max:
            raise ValueError(f'x_min must be <= x_max')
        if self.y_min > self.y_max:
            raise ValueError('y_min must be <= y_max')

    @staticmethod
    def from_dict(coordinates):
        return Area(x_min=coordinates['x_min'], x_max=coordinates['x_max'],
                    y_min=coordinates['y_min'], y_max=coordinates['y_max'])


Areas = List[Area]


@dataclass
class Camera:
    id: str
    name: str
    trigger_url: str
    ignore_areas: Areas
    homekit_acc_id: str = None

    @staticmethod
    def _contains(ignore_area, area):
        return ignore_area.x_min < area["x_min"] < area["x_max"] < ignore_area.x_max and \
               ignore_area.y_min < area["y_min"] < area["y_max"] < ignore_area.y_max

    def is_ignored(self, object_area):
        for ignore_area in self.ignore_areas:
            if self._contains(ignore_area, object_area):
                LOG.debug(f'Object ({object_area}) is inside ({ignore_area})')
                return True

        LOG.debug(f'Object ({object_area}) is outside ({self.ignore_areas})')
        return False

    def msg(self, msg):
        return f'Camera {self.id} ({self.name}): {msg}'


class CameraCollection(dict):
    def __init__(self, camera_settings: Dict):
        super().__init__()
        self._create(camera_settings)

    def __missing__(self, key):
        if isinstance(key, int):
            return self[str(key)]

        raise KeyError(f'Camera ID not found: {key}')

    def _create(self, camera_settings: Dict):
        for camera_id, settings in camera_settings.items():
            ignore_areas = []
            for area in settings.get('ignore_areas', []):
                ignore_areas.append(Area.from_dict(area))

            self[camera_id] = Camera(
                camera_id,
                settings.get('name', f'unnamed camera {camera_id}'),
                settings.get('trigger_url'),
                ignore_areas,
            )
