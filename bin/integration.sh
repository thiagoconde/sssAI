LOG_LEVEL=debug                            \
CAMERAS_JSON=tests/fixtures/cameras.json   \
SETTINGS_JSON=tests/fixtures/settings.json \
python3 -m pytest -m integration "$@"
coverage xml
